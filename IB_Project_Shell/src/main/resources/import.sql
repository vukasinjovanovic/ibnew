

truncate table authority;
truncate table users;
truncate table user_authority;

set foreign_key_checks = 1;

insert into authority (name) values ('ADMIN');
insert into authority (name) values ('REGULAR');

INSERT INTO USERS (email, password, certificate, active) VALUES ('admin@gmail.com','$2a$04$SwzgBrIJZhfnzOw7KFcdzOTiY6EFVwIpG7fkF/D1w26G1.fWsi.aK', './data/admin.jks' , 1);

insert into user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN


--DROP SCHEMA IF EXISTS ibnew;
--CREATE SCHEMA ibnew DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
--USE ibnew;

--CREATE TABLE users(
	--id INT AUTO_INCREMENT,
    --email VARCHAR(35) NOT NULL,
--	password VARCHAR(20) NOT NULL, 
--	certificate VARCHAR(100) NOT NULL, 
--	active TINYINT(1),
--	authority VARCHAR(10) NOT NULL,
  --  PRIMARY KEY(id)
--);

--INSERT INTO USERS (id, email, password, certificate, active, authority) VALUES (1, 'admin@gmail.com','$2a$10$NyaltQLE9xiwjHb9EdNFW.J8DvXrfr0wKKPXDwXzA4Cj6epBhBXou', '' , 1, 'Admin');

--CREATE TABLE authority(
--	id INT AUTO_INCREMENT,
	--name VARCHAR(10),
  --  PRIMARY KEY(id)
--);
--INSERT INTO AUTHORITY (id, name) VALUES (1, 'Regular');
--INSERT INTO AUTHORITY (id, name) VALUES (2, 'Admin');
