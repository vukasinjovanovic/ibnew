package ib.project.certificate;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import ib.project.model.IssuerData;
import ib.project.model.SubjectData;

//Klasa koja sluzi za generisanje Self-Signed sertifikata
//vlasnik sam sebi potpisuje sertifikat

public class CertificateGenerator {

	
	
	/*// Dodaje se provajder
	// Staticki blok sluzi nam za staticku inicijalizaciju klase
	// Kod se izvrsava samo jednom pri kompajliranju
	static {
		addingBouncyCastleProvider();
	}

	private static void addingBouncyCastleProvider() {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	*//**
	 * Metoda koja sluzi za generisanje sertifikata.
	 * 
	 * @param issuerData - podaci o izdavaocu sertifikata
	 * @param subjectData - podaci o fizickom ili pravnom licu kojem se izdjae sertifikat
	 * 
	 * @return X509 sertifikat
	 * 
	 *//*
	public X509Certificate generateCertificate(IssuerData issuerData, SubjectData subjectData) {
		try {
			 // Posto klasa za generisanje sertifiakta ne moze da primi direktno privatni
			 // kljuc, pravi se builder za objekat koji ce sadrzati privatni kljuc. 	
			 // Objekat ce se ce se koristitit za potpisivanje sertifikata. 
			 // Parametar konstruktora je definicija algoritma koji ce se koristi za potpisivanje sertifiakta.
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");

			// postavljanje provider-a koji se koristi - BouncyCastleProvider
			builder = builder.setProvider("BC");

			// objekat koji sadrzi privatni kljuc i koji se koristi za potpisivanje sertifikata.
			// Koristimo privatni kljuc izdavaca sertifikata!!!
			ContentSigner contentSigner = builder.build(issuerData.getPrivateKey());

			// postavljaju se podaci za generisanje sertifiakta
			X509v3CertificateBuilder certtificateBuilder = new JcaX509v3CertificateBuilder(
					issuerData.getX500name(),						// ime izdavaca sertifikata
					new BigInteger(subjectData.getSerialNumber()), 	// serijski broj sertifikata
					subjectData.getStartDate(), 						// od kog trenutka sertifikat vazi (pre ovog datuma sertifikat ne vazi)
					subjectData.getEndDate(),						// do kog trenutka sertifikat vazi (posle ovog datuma sertifikat ne vazi)
					subjectData.getX500name(), 						// ime lica kojem se sertifikat izdaje
					subjectData.getPublicKey());						// javni kljuc koji se vezuje za sertifikat
			
			// generisanje sertifikata...
			// koristimo certtificateBuilder objekat koji smo inicijalizovali sa svim potrebnim informacijama. Kao parametr build metode
			// prosledjujemo contentSigner objekat koji sadrzi privatni kljuc izdavaca sertifikata kojim se vrsi potpisivanje sertifikata 
			X509CertificateHolder certificateHolder = certtificateBuilder.build(contentSigner);

			// certtificateBuilder generise sertifikat kao objekat klase X509CertificateHolder.
			// Sada je potrebno certificateHolder konvertovati u X509 sertifikat -> za to se koristi certificateConverter objekat
			JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
			
			// postavljanje provider-a koji se koristi - BouncyCastleProvider
			certificateConverter = certificateConverter.setProvider("BC");

			// konvertuje objekat u X509 sertifikat i vraca ga kao pobvratnu vrednost metode
			return certificateConverter.getCertificate(certificateHolder);

		} catch (IllegalArgumentException | IllegalStateException | OperatorCreationException | CertificateException e){
			
			System.out.println(e);
		}
		
		return null;
	}
	
	// Metoda za generisanje para kljuceva
		// Vraca par kljuceva
	public KeyPair generateKeyPair() {
		try {
				// Generisanje para kljuceva sa algoritmom RSA
				KeyPairGenerator generatorKey = KeyPairGenerator.getInstance("RSA");

				// 1024 bitni kljuc
				generatorKey.initialize(1024);

				// generisanje para kljuceva
				KeyPair keyPair = generatorKey.generateKeyPair();

				// vraca par kljuceva
				return keyPair;
			} catch (NoSuchAlgorithmException e) {
				System.out.println(e);
			}
			return null;
		}
		
		// Metoda za generisanje sertifikata
		public X509Certificate generateCertificate(KeyPair keyPair, String mail) {

			// Datum vazenja
			Date startDate, endDate = null;
			Calendar calendar = GregorianCalendar.getInstance();
			startDate = calendar.getTime(); // sertifikat vazi od trenutnog vremena (datum kreiranja sertifikata)
			calendar.setTime(startDate);
			calendar.add(GregorianCalendar.YEAR, 2); // sertifikat traje 2 godine od datuma kreiranja
			endDate = calendar.getTime();

			// Izdavaoc sertifiata
			X500NameBuilder x500Namebuilder = new X500NameBuilder(BCStyle.INSTANCE);
			builder_addRDN(mail, x500Namebuilder);
			// Serijski broj sertifikata
			String serial = "1";

			X500Name x500Name = x500Namebuilder.build();

			// podaci za izdavaca
			IssuerData issuerdata = new IssuerData(keyPair.getPrivate(), x500Name);

			// podaci za vlasnika
			SubjectData subjectdata = new SubjectData(keyPair.getPublic(), x500Name, serial, startDate, endDate);

			// kreiranje sertifikata
			return generateCertificate(issuerdata, subjectdata);
		}
		
		private void builder_addRDN(String email, X500NameBuilder builder) {
			builder.addRDN(BCStyle.CN, "Vukasin Jovanovic");
			builder.addRDN(BCStyle.SURNAME, "Jovanovic");
			builder.addRDN(BCStyle.GIVENNAME, "Vukasin");
			builder.addRDN(BCStyle.O, "FTN");
			builder.addRDN(BCStyle.OU, "Informaciona bezbednost");
			builder.addRDN(BCStyle.C, "RS");
			builder.addRDN(BCStyle.E, email);
			builder.addRDN(BCStyle.UID, "12345");
		}
		
		private X509v3CertificateBuilder x509v3CertificateImplementation(SubjectData subjectdata) {
			X509v3CertificateBuilder certtificateBuilder = new JcaX509v3CertificateBuilder(subjectdata.getX500name(),
					new BigInteger(subjectdata.getSerialNumber()), subjectdata.getStartDate(), subjectdata.getEndDate(),
					subjectdata.getX500name(), subjectdata.getPublicKey());
			return certtificateBuilder;
		}
		
		public static void printCertificate(X509Certificate certificate) {
			System.out.println("ISSUER: " + certificate.getIssuerX500Principal().getName());
			System.out.println("SUBJECT: " + certificate.getSubjectX500Principal().getName());
			System.out.println("Sertifikat:");
			System.out.println("--------------------------------------------------------------------------------------------------------------");
			System.out.println(certificate);
			System.out.println("--------------------------------------------------------------------------------------------------------------");
			System.out.println("--------------------------------------------------------------------------------------------------------------");
		}
	
	*/
}
