package ib.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ib.project.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findByEmail(String email);
	List<User> findAll();
	User findByEmailAndPassword(String email, String password);
}
