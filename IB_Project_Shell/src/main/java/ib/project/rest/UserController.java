package ib.project.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ib.project.dto.LoginDTO;
import ib.project.model.Authority;
import ib.project.model.User;
import ib.project.security.TokenHelper;
import ib.project.service.AuthorityServiceInterface;
import ib.project.service.UserService;
import ib.project.service.UserServiceInterface;

@RestController
public class UserController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private AuthorityServiceInterface authorityService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	TokenHelper tokenHelper;
	
	
	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO){
		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword());
			
			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
			User loggedUser = userService.findByEmail(loginDTO.getUsername());
			HashMap<String,Object> retVal = new HashMap<String,Object>();
			retVal.put("token", tokenHelper.generateToken(details));
			retVal.put("email", loggedUser.getEmail());
			retVal.put("active_status", loggedUser.isActive());
			retVal.put("role", loggedUser.getAuthorities());
			
			return ResponseEntity.ok(retVal);
			
		}catch (Exception e) {
			// TODO: handle exception
            return new ResponseEntity<String>("Invalid login", HttpStatus.UNAUTHORIZED);

		}
		
	}
	
	@RequestMapping(value = "/api/logout", method = RequestMethod.POST)
	public ResponseEntity<?> logout(HttpServletRequest request){
	HttpSession session= request.getSession(false);
	    SecurityContextHolder.clearContext();
	         session= request.getSession(false);
	        if(session != null) {
	            session.invalidate();
	        }

	    return new ResponseEntity<String>("logout successful",HttpStatus.OK);
	}
	
	@GetMapping(path="/api/users")
	public List<User> findAll() {
		return userService.findAll();
	}
	
	@PostMapping(path="/api/registration")
	public ResponseEntity<?> registrationUser(@RequestBody LoginDTO loginDTO) {
		if(loginDTO.getUsername().isEmpty() || loginDTO.getUsername().length() < 4)
			return new ResponseEntity<String>("Bad username!", HttpStatus.BAD_REQUEST);
		if(loginDTO.getPassword().isEmpty() || loginDTO.getPassword().length() < 4)
			return new ResponseEntity<String>("Bad password!", HttpStatus.BAD_REQUEST);
		Authority auth = authorityService.findByName("REGULAR");
		User user = userService.findByEmail(loginDTO.getUsername());
		if (user == null) {
			user = new User();
			user.setActive(false);
			List<Authority> userAuthorities = new ArrayList<Authority>();
			userAuthorities.add(auth);
			user.setAuthorities(userAuthorities);
			user.setCertificate("");
			user.setEmail(loginDTO.getUsername());
			user.setPassword(passwordEncoder.encode(loginDTO.getPassword()));
			
			userService.save(user);
			return new ResponseEntity<String>("Account created successfully",HttpStatus.CREATED);
		}else {
			return new ResponseEntity<String>("User already exists!", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(path="/api/activate_user")
	public ResponseEntity<String> activateUser(@RequestParam Integer id) {
		User user = userService.findById(id);
		if(user == null) {
			return new ResponseEntity<String>("User don't exists!", HttpStatus.BAD_REQUEST);
		}
		else if(user.isActive() == true) {
			return new ResponseEntity<String>("User already active!", HttpStatus.BAD_REQUEST);
		}
		else {
			try {
				user.setActive(true);
				userService.save(user);
				return new ResponseEntity<String>("Account activation successful",HttpStatus.OK);
			}catch (Exception e) {
				return new ResponseEntity<>("Activation failed", HttpStatus.BAD_REQUEST);
			}
		}
	}
	
	@GetMapping(path="/api/findUser")
	public ResponseEntity<?> userEmail(@RequestHeader("email") String email) {
		User user = userService.findByEmail(email);
		if (user != null) {
			List<User> users = new ArrayList<User>();
			users.add(user);
			return new ResponseEntity<List<User>>(users,HttpStatus.OK);
		} else {
			System.out.println("User doesn't exist!");
			return new ResponseEntity<>("User with given email doesn't exist", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
	
	
	
	
	
	
	
	

}
