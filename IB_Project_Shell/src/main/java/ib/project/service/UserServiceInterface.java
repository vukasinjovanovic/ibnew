package ib.project.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ib.project.model.User;

public interface UserServiceInterface {

	User findById(int id);
	User findByEmail(String username);
	List<User> findAll ();
	User findByEmailPassword (String email, String password);
	User save(User user);
	
}
