package ib.project.service;

import org.springframework.stereotype.Service;

import ib.project.model.Authority;

public interface AuthorityServiceInterface {
	
	Authority findById (int id);
	Authority findByName(String name);

}
